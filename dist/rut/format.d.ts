declare type Format = 'dotted' | 'dashed';
export declare function format(rut: string, format?: Format): string;
export {};
