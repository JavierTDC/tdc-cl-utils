export function clean(rut: string): string {
  return rut.replace(/^0+|[^0-9kK]+/g, "").toUpperCase();
}
