import { clean } from './clean';

type Format = 'dotted' | 'dashed';

function dotted(rut: string): string {
    let result = rut.slice(-4, -1) + '-' + rut.substr(rut.length - 1);

    for (var i = 4; i < rut.length; i += 3) {
        result = rut.slice(-3 - i, -i) + '.' + result;
    }

    return result;
}

function dashed(rut: string): string {
    const length = rut.length;

    return rut.substring(0, length - 1) + '-' + rut.substring(length - 1);
}

export function format(rut: string, format: Format = 'dotted'): string {
    rut = clean(rut);

    if (format === 'dotted') {
        return dotted(rut);
    }

    return dashed(rut);
}
