## Utilidades

#### RUT

```ts
import { rut } from '@tdc-cl/utils'

rut.validate('12.312.312-3') // true
rut.format('12312312-3') // 12.312.312-3
rut.format('12312312-3', 'dashed') // 12312312-3
rut.clean('12.312.312-3') // 123123123
```

## Cómo publicar

1. Iniciar sesión `npm login`
2. Hacer cambios
3. Hacer build: `yarn tsc`
4. Publicar: `npm publish`
    - En caso de no dejar publicar, correr `npm publish --access public`
