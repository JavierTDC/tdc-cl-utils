import { validate } from "./validate";
import { format } from "./format";
import { clean } from "./clean";

export default {
  validate,
  format,
  clean,
};
