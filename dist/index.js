'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function clean(rut) {
    return rut.replace(/^0+|[^0-9kK]+/g, "").toUpperCase();
}

function validate(rut) {
    if (!/^0*(\d{1,3}(\.?\d{3})*)-?([\dkK])$/.test(rut)) {
        return false;
    }
    rut = clean(rut);
    let t = parseInt(rut.slice(0, -1), 10);
    let m = 0;
    let s = 1;
    while (t > 0) {
        s = (s + (t % 10) * (9 - (m++ % 6))) % 11;
        t = Math.floor(t / 10);
    }
    const v = s > 0 ? "" + (s - 1) : "K";
    return v === rut.slice(-1);
}

function dotted(rut) {
    let result = rut.slice(-4, -1) + '-' + rut.substr(rut.length - 1);
    for (var i = 4; i < rut.length; i += 3) {
        result = rut.slice(-3 - i, -i) + '.' + result;
    }
    return result;
}
function dashed(rut) {
    const length = rut.length;
    return rut.substring(0, length - 1) + '-' + rut.substring(length - 1);
}
function format(rut, format = 'dotted') {
    rut = clean(rut);
    if (format === 'dotted') {
        return dotted(rut);
    }
    return dashed(rut);
}

var index = {
    validate,
    format,
    clean,
};

exports.rut = index;
//# sourceMappingURL=index.js.map
